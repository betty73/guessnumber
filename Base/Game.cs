﻿using GuessNumber.Interfaces;
namespace GuessNumber.Base;

public abstract class Game<T> : IGame
{
	protected ISettings<T> _settings;
    protected T _generatedNumber;
    protected IGameInterface<T> _gameInterface;
	protected bool _won;

	public Game(
		ISettings<T> settings,
		IGenerator<T> numberGenerator,
		IGameInterface<T> gameInterface)
	{
		_settings = settings;
		_gameInterface = gameInterface;
		_generatedNumber = numberGenerator.Generate(_settings.Range);
		_won = false;
	}

	protected void ShowEndOfGameMessage()
	{
		if (_won)
		{
            ShowMessage("You are winner!!");
		}
		else
		{
            ShowMessage($"This number is : {_generatedNumber}");
		}
	}
    protected void ShowMessage(string message)
    {
        _gameInterface.Write(message);
    }
    protected virtual bool IsTheGameOver(int currentAttempt)
	{
		return !_won;
	}

	public abstract void Play();
}