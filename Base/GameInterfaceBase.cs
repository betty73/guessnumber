﻿using System.ComponentModel;
using GuessNumber.Interfaces;
namespace GuessNumber.Base;

public abstract class GameInterfaceBase<T> : IGameInterface<T>
{
    public virtual T Read()
    {
        var value = Console.ReadLine();

        TypeConverter typeConverter = TypeDescriptor.GetConverter(typeof(T));
        return (T)typeConverter.ConvertFromString(value);
    }

    public abstract void Write(string message);
}