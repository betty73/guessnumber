﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessNumber.Base;
public class Range<T>
{
    public T Max { get; set; }
    public T Min { get; set; }
}
