﻿using System.Configuration;
using GuessNumber.Interfaces;
namespace GuessNumber.Base;

public class Settings:ISettings<int>
{
    protected Range<int> _range;
    public Range<int> Range
    {
        get
        {
            if (_range is null)
            {
                _range = new ();
                _range.Min = int.Parse(
                    ConfigurationManager.AppSettings["Min"]);
                
                _range.Max = int.Parse(
                   ConfigurationManager.AppSettings["Max"]);
            }
            return _range;
        }
        private set { }
    }

    public int Attempts
    {
        get
        {
            return int.Parse(
                ConfigurationManager.AppSettings["Attempts"]);
        }
    }
}
