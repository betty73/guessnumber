﻿using GuessNumber.Base;

namespace GuessNumber;

public class ConsoleGameInterface : GameInterfaceBase<int> 
{
	public override int Read()
	{
		try
		{
			return base.Read();
		}
		catch (NullReferenceException )
		{
			return 0;
		}
        catch (Exception )
        {
            return 0;
        }
    }
	public override void Write(string message)
	{
		Console.WriteLine(message);
	}	
}