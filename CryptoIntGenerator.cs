﻿using GuessNumber;
using GuessNumber.Base;
namespace GuessNumber;

public class CryptoIntGenerator : RandomIntGenerator
{
    public override int Generate(Range<int> range)
    {
        return System.Security.Cryptography.RandomNumberGenerator.GetInt32(range.Min,range.Max + 1);
    }
}
