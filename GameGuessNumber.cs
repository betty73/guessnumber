﻿using System;
using GuessNumber.Base;
using GuessNumber.Interfaces;
namespace GuessNumber;

public class GameGuessNumber : Game<int>
{
	public GameGuessNumber(
		IGenerator<int> numberGenerator,
		IGameInterface<int> gameInterface)
		: base(new Settings(), numberGenerator, gameInterface)
	{
	}

	public override void Play()
	{
		var currentAttempt = 1;
		do
		{
			_gameInterface.Write($"Attempt {currentAttempt} from {_settings.Attempts}");
			CheckTheGuess(_gameInterface.Read());

			currentAttempt++;
		}
		while (!IsTheGameOver(currentAttempt));
    }

	private void CheckTheGuess(int userNumber)
	{
		if (userNumber == _generatedNumber)
		{
			_won = true;
		}
		else
		{
			if (userNumber < _generatedNumber)
			{
				ShowMessage("More!");
			}

			if (userNumber > _generatedNumber)
			{
				ShowMessage("Less!");
			}
		}
	}

	protected override bool IsTheGameOver(int currentAttempt)
	{
		if (!_won && currentAttempt <= _settings.Attempts)
		{
			return false;
		}
		else
		{
			ShowEndOfGameMessage();
			return true;
		}
	}
}