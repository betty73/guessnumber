﻿using System;
using GuessNumber.Base;
namespace GuessNumber;

public class GameManager
{
	private Game<int> _game;

	public GameManager(GameGuessNumber game)
	{
		_game = game;
	}

	public void Run()
	{
		_game?.Play();
	}
}