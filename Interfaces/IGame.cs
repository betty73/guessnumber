﻿namespace GuessNumber.Interfaces;

public interface IGame
{
	public void Play();
}