﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuessNumber.Base;

namespace GuessNumber.Interfaces;

public interface IGenerator<T> 
{
    public T Generate(Range<T> range);
}
