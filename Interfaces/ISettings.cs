﻿using System.Configuration;
using GuessNumber.Base;

namespace GuessNumber.Interfaces;

public interface ISettings<T>
{
	public Range<T> Range { get; }
	public int Attempts { get; }
}
