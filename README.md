Single Responsibility Principle (Принцип единственной обязанности)
Класс GameGuessNumber ответственеy за игру, логика по генерации числа вынесена в RandomIntGenarator, логика ответсвенная за взаимодействие с пользователем реализована 
в ConsoleGameInterface. Таком образом каждый класс отвечает за свою задачу.

Open/Closed Principle (Принцип открытости/закрытости)
Можно создать новые классы на основе интерфейсов IGenerator,IGameInterface,ISettings и получить другую логику работы приложения.
Классы закрыты на изменение, но открыты для расширения

Liskov Substitution Principle (Принцип подстановки Лисков)
Класс GameGuessNumber наследуется от абстрактного класса Game. Объект класса передается в GameManager

Interface Segregation Principle (Принцип разделения интерфейсов)
Интерфейс IGameInterface имплементирует: IReader, IWriter для вводы/вывода данных.

Dependency Inversion Principle (Принцип инверсии зависимостей)
Для каждого класса реализован интерфейс, зависимостей нет.
