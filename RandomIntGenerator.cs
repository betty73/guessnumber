﻿using System;
using GuessNumber.Base;
using GuessNumber.Interfaces;
namespace GuessNumber;

public class RandomIntGenerator : IGenerator<int> 
{
	public virtual int Generate(Range<int> range)
	{
		return new Random().Next(range.Min, range.Max);
	}
}